import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { nameValidator } from '../shared/user-validator';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {
  signupForm : FormGroup;

  constructor(private formBuilder : FormBuilder) { }

  ngOnInit(): void {
    
    this.signupForm = this.formBuilder.group({
      name:['',[Validators.required,nameValidator.bind(this),Validators.maxLength(20),Validators.pattern('^[a-zA-Z ]*$')]],
      email:['',Validators.required],
    })
  }
    
  nameValidation(){
    let name = this.signupForm.controls.name.value;
    console.log(name);
  }

}
